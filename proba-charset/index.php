<!DOCTYPE html>
<html lang="gl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Charset</title>
</head>
<body>
    <?php
    $texto="Ola Olá OLÁ";
    $t_lower = strtolower($texto);
    $t_upper = strtoupper($texto);
    print("<h1>strtolower e strtoupper</h1>");
    print("Texto: $texto<br>Texto lower: $t_lower<br>Texto upper: $t_upper");
    $t_lower = mb_strtolower($texto);
    $t_upper = mb_strtoupper($texto);
    print("<h1>mb_strtolower e mb_strtoupper</h1>");
    print("Texto: $texto<br>Texto lower: $t_lower<br>Texto upper: $t_upper");
    ?>
</body>
</html>