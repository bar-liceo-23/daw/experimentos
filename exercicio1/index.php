<!DOCTYPE html>
<html lang="gl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exercicio 1</title>
</head>
<body>
    <p>Crea un script en PHP que pida dos números por pantalla mediante un formulario (X e Y por ejemplo). Al enviar los datos por el formulario los compare e indique el menor con un mensaje por pantalla: “El número más pequeño entre X e Y es NÚMEROMENOR”.</p>

    <form action="index.php" method="get">
        <label for="numberX">Número X:</label>
        <input type="text" name="numberX" value="<?php 
        if (isset($_GET["numberX"])){
            print($_GET['numberX']);
        }
        ?>">
        <br>
        <label for="numberY">Número Y:</label>
        <input type="text" name="numberY" value="<?php
        if (isset($_GET["numberY"])){
            print($_GET['numberY']);
        }
        ?>">
        <br>
        <input type="submit" value="Enviar">
    </form>
    <hr>
<?php
if (isset($_GET["numberX"]) && isset($_GET["numberY"])){
    $x = $_GET["numberX"];
    $y = $_GET["numberY"];

    if ($x < $y){
        $menor = $x;
    } else {
        $menor = $y;
    }
    
    print("<p>El número más pequeño entre $x e $y es $menor</p>");
}
?>



</body>
</html>