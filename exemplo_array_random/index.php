<!DOCTYPE html>
<html lang="gl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exemplo - Arrays</title>
</head>
<body>

<form>
    <label for="numero">Numeros a xenerar</label>
    <input type="number" name="numero" value="<?php
    if (isset($_GET["numero"])){
        print($_GET["numero"]);
    } else {
        print(0);
    }
    ?>">
    <input type="submit" value="Enviar">
</form>
<?php
    print("<p>");
    if (isset($_GET["numero"])){
        $meu_array = array();
        for ($i=0; $i<$_GET["numero"]; $i++){
            $meu_array[$i] = rand(1, 200);
        }
        for ($i=0; $i<count($meu_array); $i++){
            print("Número $i: {$meu_array[$i]}</br>");
        }
        print("</p><p>");
        print_r($meu_array);
        print("</p>");
    }
?>

</body>
</html>