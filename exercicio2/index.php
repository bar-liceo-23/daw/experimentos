<!DOCTYPE html>
<html lang="gl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exercicio 2</title>
</head>
<body>
    
<p>Muestra la tabla de multiplicar de un número introducido en un
formulario. La tabla de multiplicar se ha de mostrar en una tabla html.</p>
<hr>
<?php
    $numero = '';
?>

<form>
    <label for="number">Número</label>
    <input type="number" name="numero" value="<?php 
    if (isset($_GET['numero'])){
        $numero = $_GET['numero'];
        print($numero);
    }
    ?>">
    <input type="submit" value="Enviar">
</form>
    <?php
    if(isset($_GET['numero']) && ctype_digit($numero)){
        //Agora tamén comprobamos que $numero sexa un número enteiro positivo
        print("
            <h1>Taboa de multiplicar do número $numero</h1>
            <table border='1px'>
                <tr>
                    <th>Operación</th>
                    <th>Resultado</th>
                </tr>
        ");
        for ($i=1; $i <= 10; $i++) {
            $resultado = $i*$numero; 
            print("
                <tr>
                    <td>$i x $numero</td>
                    <td>$resultado</td>
                </tr>
            ");
        }
        print("</table>");
    }
    ?>
</body>
</html>