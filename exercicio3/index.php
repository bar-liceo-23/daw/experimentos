<!DOCTYPE html>
<html lang="gl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exercicio 3</title>
    <style>
        .num-positivo{
            color:green;
        }
        .num-negativo{
            color:red;
        }
        .num-cero{
            color:grey;
        }

    </style>
</head>
<body>
    <p>Crea un script que compare 10 números generados de forma
aleatoria y nos diga cuáles son positivos y cuáles son negativos.</p>
<hr>
<?php
    $n_total = 10;
    $numeros=array();
    for($i = 0; $i<$n_total; $i++){
        array_push($numeros, rand(-100, 100));
    }
    for($i=0; $i<count($numeros); $i++){
        if($numeros[$i] > 0){
            $msg = "positivo";
            $class = "num-positivo";
        } else {
            if ($numeros[$i] < 0){
                $msg = "negativo";
                $class = "num-negativo";
            } else {
                $msg = "";
                $class = "num-cero";
            }
        }
        print("<p class='$class'>{$numeros[$i]} - $msg</p>");
    }

?>    

</body>
</html>