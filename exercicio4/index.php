<!DOCTYPE html>
<html lang="gl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exercicio 4</title>
    <style>
        .lunes{
            color:red;
        }
        .martes{
            color:blue;
        }
        .miércoles{
            color:green;
        }
        .jueves{
            color:purple;
        }
        .viernes{
            color:brown;
        }
        .dise{
            color:purple;
        }
        .desa{
            color:red;
        }
        .desp{
            color:grey;
        }
    </style>
</head>
<body>
    <h1>Exercicio 4</h1>
    <p>Escribe un script que pida por pantalla un día de la semana y que diga qué asignatura toca a primera hora ese día, el mensaje será “Hoy es lunes y vamos a estudiar NOMBREDELAMATERIA”. La información de las asignaturas ha de estar almacenada en un array asociativo.</p>
    <hr>

    <p>
    <form action="index.php" method="get">
        <label for="dia_semana">Día de la semana: </label>
        <input type="text" name="dia_semana" value="<?php 
        if (isset($_GET["dia_semana"])){
            print($_GET['dia_semana']);
        }
        ?>">
        <br>
        <input type="submit" value="Enviar">
    </form>
    </p>
    <p>
    <?php
    function print_dia_span($dia) : String {
        $horario = [
            "lunes" => "Diseño de interfaces web",
            "martes" => "Despliegue de aplicaciones web",
            "miércoles" => "Despliegue de aplicaciones web",
            "jueves" => "Despliegue de aplicaciones web",
            "viernes" => "Desarrollo de aplicaciones en entorno servidor"
        ];
        if (array_key_exists($dia, $horario)){
            $css_class = mb_strtolower(substr($horario[$dia], 0, 4));
            return "Hoy es <span class='$dia'>$dia</span> y vamos a estudiar <span class='$css_class'>$horario[$dia]</span>";
        } else{
            return "$dia non é un día da semana.";
        }        
    }

    if (isset($_GET["dia_semana"])){
        $dia = $_GET["dia_semana"];
        $dia = mb_strtolower($dia);
        print(print_dia_span($dia));
        
    }
    ?>
    </p>

</body>
</html>