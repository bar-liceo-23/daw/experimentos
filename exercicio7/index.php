<!DOCTYPE html>
<html lang="gl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exercicio 7</title>
    <style>
        div{
            margin: 10px;
        }
        .msg{
            font-weight: bold;
        }
        .autorizado{
            color: green;
        }
        .non-autorizado{
            color:red;
        }
    </style>
</head>
<body>
    <h1>Exercicio 7</h1>
    <p>Crear una aplicación web en PHP con un formulario en el que se introduzca el peso máximo autorizado de un vehículo y el peso del vehículo, al pulsar el botón de enviar deberá indicar:</p>
    <ul>
        <li>Si el vehículo pesa menos: mensaje en verde comunicando que ese vehículo está autorizado. Un segundo mensaje que indique cantos kilogramos sobran para llegar al peso máximo.</li>
        <li>Si el vehículo pesa más: hay que indicar que ese vehículo no está autorizado con un mensaje en rojo. También hay que notificar el exceso de kilogramos.</li>
    </ul>
    <hr>
    
    <form action="index.php" method="POST">
        <div>
            <label for="peso_maximo">Peso Máximo Autorizado:</label>
            <input type="number" name="peso_maximo" value="<?php
                if(isset($_POST['peso_maximo'])){
                    print($_POST['peso_maximo']);
                }
            ?>">
        </div>
        <div>
            <label for="peso_vehiculo">Peso Vehículo:</label>
            <input type="number" name="peso_vehiculo" value="<?php
                if(isset($_POST['peso_vehiculo'])){
                    print($_POST['peso_vehiculo']);
                }
            ?>">
        </div>

        <div>
            <input type="submit" value="Enviar">
        </div>
    </form>

    <div>
        <?php
        function get_string_to_print($peso, $peso_maximo) : String {
            $exceso = abs($peso - $peso_maximo);
            if ($peso <= $peso_maximo){
                $msg = "VEHÍCULO AUTORIZADO";
                $clase = "autorizado";
                $msg_exceso = "Fáltanche";
            } else{
                $msg = "VEHÍCULO NON AUTORIZADO";
                $clase = "non-autorizado";
                $msg_exceso = "Pásaste";
            }
            return "
                <p class='msg $clase'>$msg</p>
                <p>$msg_exceso $exceso Quilogramos</p>
                ";
        }

        if(isset($_POST['peso_vehiculo'], $_POST['peso_maximo'])){
            print(get_string_to_print($_POST['peso_vehiculo'], $_POST['peso_maximo']));
        }
        ?>
    </div>
</body>
</html>